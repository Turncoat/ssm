#
# Sim Server Manager - Simple server manager for sim racing titles.
# copyright (c) 2019 Turncoat Tony
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import sys
import getopt

#class
def main(argv):
    usage = ( #ndlkawndlkwa
                "Usage: "+ argv[0] +" [-h] [-d] [-p] [-r] [-q] [-v]\n"
			    "  -h    --help               Display this usage information\n"
			    "  -d    --drift              Read from drift configuration\n"
			    "  -p    --practice           Read from practice configuration\n"
			    "  -r    --race               Read from race configuration (\033[37;1mdefault\033[37;0m)\n"
			    "  -q    --quiet              Don't display logging to console\n"
                "  -v    --version            Display version\n"
            )

    # Runtime configuration options.  Use GNU getopt so we can use mutliple flags.
    long_opts = [  "help", "drift", "practice", "race", "quiet", "version", "verbose"]
    opts, args = getopt.gnu_getopt(sys.argv[1:], "hdprqvV", long_opts)

    # Holy shit, if/else blocks for this is ugly as shit.
    output = None
    quiet = False
    for o, a in opts:
        if o == "-V":
            verbose = True
        elif o in ("-h", "--help"):
            print(usage)
            sys.exit()
        elif o in ("-d", "--drift"):
            output = a
        elif o in ("-p", "--practice"):
            output = a
        elif o in ("-r", "--race"):
            output = a
        elif o in ("-q", "--quiet"):
            output = a
        elif o in ("-v", "--version"):
            output = a
        else:
            assert False, "unhandled option"
    print(output)

if __name__ == "__main__":
    main(sys.argv)
